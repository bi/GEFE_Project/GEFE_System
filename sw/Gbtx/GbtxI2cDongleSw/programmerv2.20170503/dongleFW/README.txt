PC host application for USB-I2C dongle
--------------------------------------

D Porret - PH/ESE - 08/09/2015

based on V-USB bootloadHID example (http://www.obdev.at)

dongle MUST have the following USB decriptors / Ids in the firmware usbdrv.h to be flashed.

#define IDENT_BOOT_VENDOR_NUM        0x20A0

#define IDENT_BOOT_PRODUCT_NUM       0x4110

#define IDENT_BOOT_VENDOR_STRING     "CERN"

#define IDENT_BOOT_PRODUCT_STRING    "Boot"


to compile the source on windows, install MinGW and MSYS and do a make command
MinGW and MSYS: http://www.mingw.org/

Usage from console window:
bootload.exe �f
-	check if a dongle in bootloader mode is connected to the host computer.
bootload.exe -r usb_i2c_xx.hex
-	write the dongle flash memory with the content of the hex file.

* The GBTx I2C dongle control software and the golden configuration of the GBTx (2015_02_17_MinimalConfig.txt) may be acceses through the following link:

  https://espace.cern.ch/GBT-Project/VLDB/Control/Forms/AllItems.aspx

* Tutorials about how to configure and program the GBTx are available through the following link:\

  - How to install the GBTXProgrammer:

    https://cds.cern.ch/record/2203822?ln=en 
  
  - How to update the dongle:
  
    https://cds.cern.ch/record/2203805?ln=en 
  
  - How to get started with the GBTXProgrammer:

    https://cds.cern.ch/record/2207601?ln=en 

  - More short video tutorials will soon follow.


